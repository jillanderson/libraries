#include <common/key_press_type_detector.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

enum key_detector_internal_state {
	KEY_DETECTOR_STATE_00 = 0U,
	KEY_DETECTOR_STATE_10 = 10U,
	KEY_DETECTOR_STATE_20 = 20U,
	KEY_DETECTOR_STATE_30 = 30U,
	KEY_DETECTOR_STATE_40 = 40U
};

void key_press_event_detector_init(key_press_event_detector_st *const detector,
		uint16_t double_press_time, uint16_t longpress_start_time,
		uint16_t longpress_repeat_time)
{
	detector->double_press_time = double_press_time;
	detector->longpress_start_time = longpress_start_time;
	detector->longpress_repeat_time = longpress_repeat_time;
	detector->press_time_cnt = 0U;
	detector->longpress_repeat_time_cnt = 0U;
	detector->callback = NULL;
	detector->arg = NULL;
	detector->event = KEY_NOT_PRESSED_EV;
	detector->internal_state = KEY_DETECTOR_STATE_00;
}

void key_press_event_detector_handler(
		key_press_event_detector_st *const detector,
		key_press_handler_ft *handler, void *arg)
{
	detector->callback = handler;
	detector->arg = arg;
}

void key_press_event_detector_dispatch(
		key_press_event_detector_st *const detector)
{
	if (KEY_NOT_PRESSED_EV != detector->event) {
		if (NULL != detector->callback) {
			(detector->callback)(detector->event, detector->arg);
		}

		detector->event = KEY_NOT_PRESSED_EV;
	}
}

void key_press_event_detector_on_tick_time(
		key_press_event_detector_st *const detector,
		bool key_is_pressed)
{
	/* obsługa licznika czasu */
	if (KEY_DETECTOR_STATE_00 != detector->internal_state) {
		detector->press_time_cnt++;
	}

	switch (detector->internal_state) {
	case KEY_DETECTOR_STATE_00:
		/* czekamy na wciśnięcie przycisku */
		if (true == key_is_pressed) {
			detector->press_time_cnt = 0U;
			detector->internal_state = KEY_DETECTOR_STATE_10;
		}

		break;

	case KEY_DETECTOR_STATE_10:
		/* czekamy na zwolnienie przycisku */
		if (false == key_is_pressed) {
			detector->internal_state = KEY_DETECTOR_STATE_20;
		} else {
			/* wykrycie długiego wciśnięcia */
			if ((true == key_is_pressed) && ((detector->press_time_cnt)
						> (detector->longpress_start_time))) {
				detector->event = KEY_LONGPRESS_START_EV;
				detector->internal_state = KEY_DETECTOR_STATE_40;
			}
		}

		break;

	case KEY_DETECTOR_STATE_20:
		/* czekamy na przekroczenie czasu oczekiwania lub ponownie
		 wciśnięcie przycisku (dwuklik) */
		if ((detector->press_time_cnt) > (detector->double_press_time)) {
			/* to było pojedyńcze wciśnięcie */
			detector->event = KEY_SHORT_PRESSED_EV;
			detector->internal_state = KEY_DETECTOR_STATE_00;
		} else {
			/* jeśli ponownie wciśnięty przycisk przed upływem czasu
			 oczekiwania na dwuklik */
			if (true == key_is_pressed) {
				detector->internal_state = KEY_DETECTOR_STATE_30;
			}
		}

		break;

	case KEY_DETECTOR_STATE_30:
		/* oczekiwanie na ostateczne zwolnienie przycisku */
		if (false == key_is_pressed) {
			/* to był dwuklik */
			detector->event = KEY_DOUBLE_PRESSED_EV;
			detector->internal_state = KEY_DETECTOR_STATE_00;
		}

		break;

	case KEY_DETECTOR_STATE_40:
		/* oczekiwanie na zwolnienie przycisku po długim wciśnięciu */
		if (false == key_is_pressed) {
			detector->event = KEY_LONGPRESS_STOP_EV;
			detector->longpress_repeat_time_cnt = 0U;
			detector->internal_state = KEY_DETECTOR_STATE_00;
		} else {
			/* śledzenie długiego wciśnięcia,
			 uaktualnij licznik powtórzeń */
			if (0U < (detector->longpress_repeat_time_cnt)) {
				(detector->longpress_repeat_time_cnt)--;
			}

			if (0U == (detector->longpress_repeat_time_cnt)) {
				/* wykonanie akcji cyklicznej przy długim
				 wciśnięciu */
				detector->event = KEY_LONGPRESSED_EV;
				detector->longpress_repeat_time_cnt =
						detector->longpress_repeat_time;
			}
		}

		break;

	default:
		/* nie powinno się zdarzyć - reset maszyny stanów */
		detector->event = KEY_NOT_PRESSED_EV;
		detector->press_time_cnt = 0U;
		detector->internal_state = KEY_DETECTOR_STATE_00;
		break;
	}
}

