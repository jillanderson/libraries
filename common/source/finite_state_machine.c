#include <common/finite_state_machine.h>

static fsm_msg_st const start_msg = (fsm_msg_st ) { .event = FSM_START_EV };
static fsm_msg_st const entry_msg = (fsm_msg_st ) { .event = FSM_ENTRY_EV };
static fsm_msg_st const exit_msg = (fsm_msg_st ) { .event = FSM_EXIT_EV };

void fsm_state_ctor(fsm_state_st *const state, fsm_event_handler_ft *const handler)
{
	state->handler = handler;
}

void fsm_ctor(fsm_st *const me, fsm_state_st *const state)
{
	me->current = state;
}

void fsm_on_start(fsm_st *const me)
{
	(*(me->current)->handler)(me, &entry_msg);
	(*(me->current)->handler)(me, &start_msg);
}

void fsm_on_event(fsm_st *const me, fsm_msg_st const *const msg)
{
	(*(me->current)->handler)(me, msg);
}

void fsm_state_transition(fsm_st *const me, fsm_state_st *const target)
{
	(*(me->current)->handler)(me, &exit_msg);
	me->current = target;
	(*(me->current)->handler)(me, &entry_msg);
}

