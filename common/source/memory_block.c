#include <stddef.h>
#include <stdint.h>
#include <string.h>
#include "../memory_block.h"

void* memory_block_flash_to_ram(void *restrict dst,
                void const __flash *restrict src, size_t size)
{
	uint8_t *dst_p = dst;
	uint8_t const __flash *src_p = src;

	while (size--) {
		*(dst_p++) = *(src_p++);
	}

	return (dst);
}

void* memory_block_ram_to_ram(void *restrict dst, void const *restrict src,
                size_t size)
{
	uint8_t *dst_p = dst;
	uint8_t const *src_p = src;

	while (size--) {
		*(dst_p++) = *(src_p++);
	}

	return (dst);
}

