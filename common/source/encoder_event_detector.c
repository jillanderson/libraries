#include <common/encoder_event_detector.h>
#include <stddef.h>
#include <stdint.h>

enum {
	ENTRY,
	CW1,
	CW2,
	CW3,
	CCW1,
	CCW2,
	CCW3
};

enum {
	CCW_FLAG = 0x40U,
	CW_FLAG = 0x80U
};

enum {
	VALID_CODE_NUMBER = 4U
};

/* tablica przejść maszyny stanów dekodera zdarzeń */
static uint8_t const __flash state_transitions[][VALID_CODE_NUMBER] = {
/* 00	 01    10    11 */
/* start */
{ ENTRY, CW1, CCW1, ENTRY },
/* CW_STEP1 */
{ CW2, CW1, ENTRY, ENTRY },
/* CW_STEP2 */
{ CW2, CW1, CW3, ENTRY },
/* CW_STEP3 */
{ CW2, ENTRY, CW3, (ENTRY | CW_FLAG) },
/* CCW_STEP1 */
{ CCW2, ENTRY, CCW1, ENTRY },
/* CCW_STEP2 */
{ CCW2, CCW3, CCW1, ENTRY },
/* CCW_STEP3 */
{ CCW2, CCW3, ENTRY, (ENTRY | CCW_FLAG) } };

void encoder_event_detector_init(encoder_event_detector_st *const detector)
{
	detector->callback = NULL;
	detector->arg = NULL;
	detector->status = ENTRY;
	detector->event = ENCODER_NONE_EVENT;
}

void encoder_event_detector_handler(encoder_event_detector_st *const detector,
		encoder_event_handler_ft *handler, void *arg)
{
	detector->callback = handler;
	detector->arg = arg;
}

void encoder_event_detector_dispatch(encoder_event_detector_st *const detector)
{
	if (ENCODER_NONE_EVENT != detector->event) {
		if (NULL != detector->callback) {
			(detector->callback)(detector->event, detector->arg);
		}

		detector->event = ENCODER_NONE_EVENT;
	}
}

void encoder_event_detector_time_tick(encoder_event_detector_st *const detector,
		uint8_t gray)
{
	static uint8_t const status_mask = 0x3FU;
	static uint8_t const direction_mask = 0xC0U;

	detector->status = state_transitions[detector->status & status_mask][gray];

	switch (detector->status & direction_mask) {
	case CW_FLAG:
		detector->event = ENCODER_CW_EVENT;
		break;

	case CCW_FLAG:
		detector->event = ENCODER_CCW_EVENT;
		break;

	default:
		detector->event = ENCODER_NONE_EVENT;
		break;
	}
}

