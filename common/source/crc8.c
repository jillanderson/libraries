#include <stdbool.h>
#include <stdint.h>
#include "../crc8.h"

static uint8_t const __flash ds_crc2x16_table[] = { 0x00, 0x5E, 0xBC, 0xE2,
                0x61, 0x3F, 0xDD, 0x83, 0xC2, 0x9C, 0x7E, 0x20, 0xA3, 0xFD,
                0x1F, 0x41, 0x00, 0x9D, 0x23, 0xBE, 0x46, 0xDB, 0x65, 0xF8,
                0x8C, 0x11, 0xAF, 0x32, 0xCA, 0x57, 0xE9, 0x74 };

uint8_t crc8_generate(uint8_t const *addr, uint8_t length)
{
	static uint8_t const lower_nibble_mask = 0x0FU;
	static uint8_t const array_offset = 16U;
	static uint8_t const lower_nibble_shift = 4U;

	uint8_t crc = 0U;

	while (length--) {
		crc = *addr++ ^ crc;
		crc = ds_crc2x16_table[crc & lower_nibble_mask]
		                ^ ds_crc2x16_table[array_offset + ((crc >> lower_nibble_shift) & lower_nibble_mask)];
	}

	return (crc);
}

bool crc8_is_match(uint8_t const *addr, uint8_t length, uint8_t checksum)
{
	return ((checksum == crc8_generate(addr, length)) ? true : false);
}

