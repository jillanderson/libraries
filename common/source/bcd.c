#include <stdint.h>
#include "../bcd.h"

uint16_t bcd2bin(uint8_t val)
{
	uint16_t bin = (val & 0x0FU) + (val >> 4U) * 10U;

	return (bin);
}

uint8_t bin2bcd(uint16_t val)
{
	uint8_t bcd = (uint8_t) (((val / 10U) << 4U) + val % 10U);

	return (bcd);
}

