#pragma once

/** \file **************************************************************************************************************
 *
 * generator kodu gray'a
 *
 **********************************************************************************************************************/

#include <common/gcc_attributes.h>
#include <stdint.h>

/*
 * generuje kod gray'a
 *
 * @data	stan kanału A (data)
 * @clk		stan kanału B (clk)
 * @ret		wynik w kodzie graya
 */
__gcc_static_inline uint8_t gray_code(uint8_t data, uint8_t clk)
{
	uint8_t code = 0U;

	if (0U != data) {
		code |= (1U << 1U);
	}

	if (0U != clk) {
		code |= (1U << 0U);
	}

	return (code);
}

