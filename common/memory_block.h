#pragma once

/** \file **********************************************************************
 *
 * kopiuje obszary pamięci podobnie jak funkcja memcpy_P(), z wyjątkiem,
 * że ciąg źródłowy zdefiniowany jest z modyfikatorem __flash
 *
 ******************************************************************************/

#include <common/gcc_attributes.h>
#include <stddef.h>

/*
 * kopiuje obszary pamięci podobnie jak funkcja memcpy_P(), z wyjątkiem,
 * że ciąg źródłowy zdefiniowany jest z modyfikatorem __flash
 *
 * @dst		wskaźnik do docelowego obszaru pamięci
 * @src		wskaźnik do źródłowego obszaru pamięci __flash
 * @size	rozmiar kopiowanego obszaru pamięci
 * @ret		zwraca wskaźnik do dst
 */
void* memory_block_flash_to_ram(void *restrict dst,
                void const __flash *restrict src, size_t size) __gcc_nonnull;

/*
 * kopiuje obszary pamięci jak funkcja memcpy()
 *
 * @dst		wskaźnik do docelowego obszaru pamięci
 * @src		wskaźnik do źródłowego obszaru pamięci
 * @size	rozmiar kopiowanego obszaru pamięci
 * @ret		zwraca wskaźnik do dst
 */
void* memory_block_ram_to_ram(void *restrict dst, void const *restrict src,
                size_t size) __gcc_nonnull;

