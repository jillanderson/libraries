#pragma once

/** \file **************************************************************************************************************
 *
 * makra bsługujące konwersje BCD
 *
 **********************************************************************************************************************/

#include <common/gcc_attributes.h>
#include <stdint.h>

/*
 * konwersja liczby bcd na bin
 */
uint16_t bcd2bin(uint8_t val) __gcc_const_attr;

/*
 * konwersja liczby bin na bcd
 */
uint8_t bin2bcd(uint16_t val) __gcc_const_attr;

