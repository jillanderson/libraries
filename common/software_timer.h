#pragma once

/** \file **********************************************************************
 *
 * timer programowy One-Shot
 *
 ******************************************************************************/

#include <stddef.h>
#include <stdint.h>

#include "gcc_attributes.h"

typedef void software_timer_handler_ft(void *arg);

typedef struct {
	volatile uint16_t counter;
	software_timer_handler_ft *callback;
	void *arg;
	volatile enum {
		TIMER_STOPPED,
		TIMER_RUNNING
	} status;
} software_timer_st;

/*
 * inicjuje timer
 *
 * @timer	wskaźnik do struktury timera
 */
__gcc_static_inline __gcc_nonnull void software_timer_init(
		software_timer_st *const timer)
{
	timer->counter = 0U;
	timer->callback = NULL;
	timer->arg = NULL;
	timer->status = TIMER_STOPPED;
}

/*
 * uruchamia odliczanie timera
 *
 * @timer	wskaźnik do struktury timera
 * @time	wartość do odliczenia
 * @handler	funkcja zwrotna wykonywana po zakończeniu odliczania
 * @arg		argument funkcji zwrotnej
 */
__gcc_static_inline __gcc_nonnull_args(1,3) void software_timer_start(
		software_timer_st *const timer, uint16_t time,
		software_timer_handler_ft *handler, void *arg)
{
	timer->counter = time;
	timer->callback = handler;
	timer->arg = arg;
	timer->status = TIMER_RUNNING;
}

/*
 * odlicza jednostki czasu timera
 * funkcja powinna być wywoływana cyklicznie okres pomiędzy wywołaniami
 * tej funkcji definiuje jednostkę czasu dla timera
 *
 * @timer	wskaźnik do struktury timera
 */

__gcc_static_inline __gcc_nonnull void software_timer_on_tick_time(
		software_timer_st *const timer)
{
	if (TIMER_RUNNING == timer->status) {
		if (0U < timer->counter) {
			timer->counter--;
		}

		if (0U == timer->counter) {
			(*(timer->callback))(timer->arg);
			timer->status = TIMER_STOPPED;
		}
	}
}

