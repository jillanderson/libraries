#pragma once

/** \file **********************************************************************
 *
 * skończona maszyna stanów
 *
 ******************************************************************************/

#include <common/gcc_attributes.h>
#include <stdint.h>

/*
 * definicja wiadomości i zdarzeń bazowej maszyny stanów
 *
 * dla maszyny stanów użytkownika dodatkowe parametry dodajemy przez
 * dziedziczenie tej struktury i zdefiniowanie w nowej strukturze
 * dodatkowych parametrów
 *
 * struct {
 * 	fsm_msg_st msg;
 * 	< dodatkowe parametry >
 * };
 */
typedef struct fsm_msg {
	uint8_t event;
} fsm_msg_st;

/*
 * podstawowe eventy bazowej maszyny stanów
 *
 * dla maszyny stanów użytkownika dodatkowe eventy
 * dodajemy przez ich zdefiniowanie w typie wyliczeniowym
 * począwszy od FSM_FIRST_USER_EV
 *
 * enum user_events {
 * 	USER_EVENT_0 = FSM_FIRST_USER_EV, // pierwszy dozwolony sygnał
 * 	USER_EVENT_1,
 * 	...
 * 	USER_EVENT_N
 * };
 */
enum fsm_event {
	FSM_START_EV,
	FSM_ENTRY_EV,
	FSM_EXIT_EV,
	FSM_FIRST_USER_EV /* pierwsza dozwolona wartość eventu użytkownika */
};

/*
 * bazowa maszyna stanów
 */
typedef struct fsm fsm_st;

/*
 * handler obsługi zdarzeń
 */
typedef void fsm_event_handler_ft(fsm_st *fsm, fsm_msg_st const *const msg);

/*
 * stan maszyny
 */
typedef struct fsm_state {
	fsm_event_handler_ft *handler;
} fsm_state_st;

/*
 * bazowa maszyna stanów
 */
struct fsm {
	fsm_state_st *current;
};

/*
 * konstruktor przypisujący handler obsługi do zdefiniowanego stanu
 */
void fsm_state_ctor(fsm_state_st *const state, fsm_event_handler_ft *const handler) __gcc_nonnull;

/*
 * konstruktor przypisujący początkowy stan maszyny
 */
void fsm_ctor(fsm_st *const me, fsm_state_st *const state) __gcc_nonnull;

/*
 * uruchamia maszynę stanów i wykonuje obsługę zdarzenia FSM_START_EV
 */
void fsm_on_start(fsm_st *const me) __gcc_nonnull;

/*
 * silnik fsm, przetwarza wiadomości maszyny stanów
 */
void fsm_on_event(fsm_st *const me, fsm_msg_st const *const msg) __gcc_nonnull;

/*
 * wykonuje przejście od stanu bieżącego do zadanego
 */
void fsm_state_transition(fsm_st *const me, fsm_state_st *const target) __gcc_nonnull;

