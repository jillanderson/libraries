#pragma once

#include <common/gcc_attributes.h>
#include <stdint.h>
#include <stdbool.h>

/*
 * Compute a Dallas Semiconductor 8 bit CRC, these are used in the
 * ROM and scratchpad registers.
 * Dow-CRC using polynomial X^8 + X^5 + X^4 + X^0
 */
uint8_t crc8_generate(const uint8_t *addr, uint8_t length) __gcc_nonnull __gcc_pure;

/*
 * checks if the checksum matches
 */
bool crc8_is_match(const uint8_t *addr, uint8_t length, uint8_t checksum) __gcc_nonnull __gcc_pure;

