#pragma once

/** \file **********************************************************************
 *
 * bufor kołowy
 *
 * rozmiar bufora musi być potęgą 2 (szybka arytmetyka indeksów)
 *
 ******************************************************************************/

#include <stdbool.h>
#include <stdint.h>

#include "gcc_attributes.h"

typedef struct {
	volatile uint8_t head;
	volatile uint8_t tail;
	uint8_t size;
	volatile uint8_t count;
	uint8_t *data;
} ring_buffer_st;

typedef enum {
	RING_BUFFER_SUCCESS,
	RING_BUFFER_FAIL
} ring_buffer_status_et;

/*
 * inicjuje bufor
 *
 * @buffer	wskaźnik do bufora kołowego
 * @data	wskaźnik do kontenera przechowującego dane
 * @size	rozmiar kontenera przechowującego dane, musi być potęgą 2
 */
__gcc_static_inline __gcc_nonnull void ring_buffer_init(
                ring_buffer_st *const buffer, uint8_t *const data, uint8_t size)
{
	buffer->head = 0U;
	buffer->tail = 0U;
	buffer->size = size;
	buffer->count = 0U;
	buffer->data = data;
}

/*
 * reset, wyczyszczenie bufora
 *
 * @buffer	wskaźnik do bufora kołowego
 */
__gcc_static_inline __gcc_nonnull void ring_buffer_reset(
                ring_buffer_st *const buffer)
{
	buffer->head = 0U;
	buffer->tail = 0U;
	buffer->count = 0U;
}

/*
 * całkowita pojemność bufora
 *
 * @buffer	wskaźnik do bufora kołowego
 * @ret		całkowita pojemność bufora
 */
__gcc_static_inline __gcc_nonnull uint8_t ring_buf_capacity(
                ring_buffer_st const *const buffer)
{
	return (buffer->size);
}

/*
 * ilość elementów znajdujących się w buforze
 *
 * @buffer	wskaźnik do bufora kołowego
 * @ret		ilość elementów w buforze
 */
__gcc_static_inline __gcc_nonnull uint8_t ring_buffer_count(
                ring_buffer_st const *const buffer)
{
	return (buffer->count);
}

/*
 * ilość wolnego miejsca w buforze
 *
 * @buffer	wskaźnik do bufora kołowego
 * @ret		ilość wolnego miejsca w buforze
 */
__gcc_static_inline __gcc_nonnull uint8_t ring_buffer_space(
                ring_buffer_st const *const buffer)
{
	return ((uint8_t) (buffer->size - buffer->count));
}

/*
 * testuje czy bufor jest pusty
 *
 * @buffer	wskaźnik do bufora kołowego
 * @ret		true - bufor pusty
 * 		false - bufor przechowuje dane
 */
__gcc_static_inline bool __gcc_nonnull ring_buffer_is_empty(
                ring_buffer_st const *const buffer)
{
	uint8_t tmp = false;

	if (0U == buffer->count) {
		tmp = true;
	}

	return (tmp);
}

/*
 * testuje czy bufor jest pełny
 *
 * @buffer	wskaźnik do bufora kołowego
 * @ret		true - bufor pełny
 * 		false - jest dostępne miejsce w buforze
 */
__gcc_static_inline bool __gcc_nonnull ring_buffer_is_full(
                ring_buffer_st const *const buffer)
{
	uint8_t tmp = false;

	if (buffer->size == buffer->count) {
		tmp = true;
	}

	return (tmp);
}

/*
 * odczytuje element z bufora jeśli bufor nie jest pusty
 *
 * @buffer	wskaźnik do bufora kołowego
 * @ret		RING_BUFFER_SUCCESS - dana została odczytana
 * 		RING_BUFFER_FAIL - bufor jest pusty
 */
__gcc_static_inline __gcc_nonnull ring_buffer_status_et ring_buffer_get(
                ring_buffer_st *const buffer, uint8_t *data)
{
	ring_buffer_status_et status = RING_BUFFER_FAIL;

	if (0U < buffer->count) {
		*data = buffer->data[buffer->tail];
		buffer->tail = (uint8_t) ((buffer->tail + 1U)
		                & (buffer->size - 1U));
		buffer->count = (uint8_t) (buffer->count - 1U);
		status = RING_BUFFER_SUCCESS;
	}

	return (status);
}

/*
 * zapisuje element do bufora, jeśli bufor jest pełny nadpisuje dane
 *
 * @buffer	wskaźnik do bufora kołowego
 */
__gcc_static_inline __gcc_nonnull void ring_buffer_overwrite_put(
                ring_buffer_st *const buffer, uint8_t data)
{
	if (buffer->size == buffer->count) {
		buffer->tail = (uint8_t) ((buffer->tail + 1U)
		                & (buffer->size - 1U));
		buffer->count = (uint8_t) (buffer->count - 1U);
	}

	buffer->data[buffer->head] = data;
	buffer->head = (uint8_t) ((buffer->head + 1U) & (buffer->size - 1U));
	buffer->count = (uint8_t) (buffer->count + 1U);
}

/*
 * zapisuje element do bufora, jeśli bufor jest pełny odrzuca dane
 *
 * @buffer	wskaźnik do bufora kołowego
 * @ret		RING_BUFFER_SUCCESS - dana została zapisana
 * 		RING_BUFFER_FAIL - bufor jest pełny, dana odrzucona
 */
__gcc_static_inline __gcc_nonnull ring_buffer_status_et ring_buffer_reject_put(
                ring_buffer_st *const buffer, uint8_t data)
{
	ring_buffer_status_et status = RING_BUFFER_FAIL;

	if (buffer->size > buffer->count) {
		buffer->data[buffer->head] = data;
		buffer->head = (uint8_t) ((buffer->head + 1U)
		                & (buffer->size - 1U));
		buffer->count = (uint8_t) (buffer->count + 1U);
		status = RING_BUFFER_SUCCESS;
	}

	return (status);
}

