#pragma once

/** \file **********************************************************************
 *
 * key integrator debouncer
 *
 ******************************************************************************/

#include <common/gcc_attributes.h>
#include <stdbool.h>
#include <stdint.h>

typedef enum {
	KEY_INTEGRATOR_RELEASED,
	KEY_INTEGRATOR_PRESSED
} key_integrator_state_et;

typedef struct {
	uint8_t steps_nr;
	volatile uint8_t counter;
	volatile key_integrator_state_et state;
} key_integrator_st;

/*
 * inicjuje filtr
 *
 * @integrator		wskaźnik do struktury intagratora
 * @steps		ilość kroków integratora
 */
__gcc_always_inline __gcc_nonnull void key_integrator_filter_init(
		key_integrator_st *const integrator, uint8_t steps)
{
	integrator->steps_nr = steps;
	integrator->counter = 0U;
	integrator->state = KEY_INTEGRATOR_RELEASED;
}

/*
 * zwraca aktualny stan przycisku
 *
 * @integrator	wskaźnik do struktury intagratora
 * @ret		KEY_INTEGRATOR_RELEASED, KEY_INTEGRATOR_PRESSED
 */
__gcc_always_inline __gcc_nonnull key_integrator_state_et key_integrator_filter_read_state(
		key_integrator_st *const integrator)
{
	if (0U == integrator->counter) {
		integrator->state = KEY_INTEGRATOR_RELEASED;
	} else if ((integrator->steps_nr) == integrator->counter) {
		integrator->state = KEY_INTEGRATOR_PRESSED;
	}

	return (integrator->state);
}

/*
 * uaktualnia stan integratora
 * funkcja powinna być wywoływana cyklicznie, okres pomiędzy wywołaniami
 * tej funkcji definiuje jednostkę czasu
 *
 * @integrator		wskaźnik do struktury intagratora
 * @hw_key_is_pressed	sprzętowy stan przycisku
 */
__gcc_always_inline __gcc_nonnull void key_integrator_filter_on_tick_time(
		key_integrator_st *const integrator, bool hw_key_is_pressed)
{
	if (false == hw_key_is_pressed) {
		if (0U < integrator->counter) {
			integrator->counter--;
		}
	} else {
		if (integrator->steps_nr > integrator->counter) {
			integrator->counter++;
		}
	}
}

