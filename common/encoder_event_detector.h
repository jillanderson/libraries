#pragma once

/** \file **********************************************************************
 *
 * wykrywa zdarzenia encodera
 * (steps per notch 4)
 *
 ******************************************************************************/

#include <common/gcc_attributes.h>
#include <stdint.h>

typedef enum {
	ENCODER_NONE_EVENT,
	ENCODER_CW_EVENT,
	ENCODER_CCW_EVENT
} encoder_event_et;

typedef void encoder_event_handler_ft(encoder_event_et event, void *arg);

typedef struct {
	encoder_event_handler_ft *callback;
	void *arg;
	uint8_t status;
	encoder_event_et event;
} encoder_event_detector_st;

/*
 * inicjuje detektor zdarzeń
 *
 * @detector		wskaźnik do detektora
 */
void encoder_event_detector_init(encoder_event_detector_st *const detector) __gcc_nonnull;

/*
 * rejestruje funkcję zwrotną
 *
 * @detector	wskaźnik do dekodera
 * @handler	funkcja zwrotna obsługująca wykryte zdarzenia
 * @arg		wskaźnik do argumentu handlera
 */
void encoder_event_detector_handler(encoder_event_detector_st *const detector,
		encoder_event_handler_ft *handler, void *arg) __gcc_nonnull_args(1);

/*
 * przetwarza zarejestrowane zadania
 * musi być wywoływana w pętli głównej programu
 *
 * @detector	wskaźnik do detektora
 */
void encoder_event_detector_dispatch(encoder_event_detector_st *const detector) __gcc_nonnull;

/*
 * uaktualnia stan detektora
 * funkcja powinna być wywoływana cyklicznie, okres pomiędzy wywołaniami
 * tej funkcji definiuje jednostkę czasu
 *
 * @detector	wskaźnik do detektora
 * @gray	sprzętowy stan encodera w kodzie graya
 */
void encoder_event_detector_time_tick(encoder_event_detector_st *const detector,
		uint8_t gray) __gcc_nonnull;

