#pragma once

/** \file **********************************************************************
 *
 * wykrywa rodzaj wciśnięcia przycisku
 *
 ******************************************************************************/

#include <common/gcc_attributes.h>
#include <stdbool.h>
#include <stdint.h>

typedef enum {
	KEY_NOT_PRESSED_EV,
	KEY_SHORT_PRESSED_EV,
	KEY_DOUBLE_PRESSED_EV,
	KEY_LONGPRESS_START_EV,
	KEY_LONGPRESSED_EV,
	KEY_LONGPRESS_STOP_EV,
} key_press_event_et;

typedef void key_press_handler_ft(key_press_event_et event, void *arg);

typedef struct {
	uint16_t double_press_time;
	uint16_t longpress_start_time;
	uint16_t longpress_repeat_time;
	volatile uint16_t press_time_cnt;
	volatile uint16_t longpress_repeat_time_cnt;
	key_press_handler_ft *callback;
	void *arg;
	volatile key_press_event_et event;
	volatile uint8_t internal_state;
} key_press_event_detector_st;

/*
 * inicjuje detektor zdarzeń
 *
 * @detector			wskaźnik do detektora
 * @double_press_time		czas oczekiwania na dwuklik
 * @start_longpress_time	czas wykrycia długiego wciśnięcia
 * @longpress_repeat_time	czas repetycji przy długim wciśnięciu
 */
void key_press_event_detector_init(key_press_event_detector_st *const detector,
		uint16_t double_press_time, uint16_t longpress_start_time,
		uint16_t longpress_repeat_time) __gcc_nonnull;

/*
 * rejestruje funkcję zwrotną
 *
 * @detector	wskaźnik do detektora
 * @handler	funkcja zwrotna obsługująca wykryte zdarzenia
 * @arg		wskaźnik do argumentu funkcji zwrotnej
 */
void key_press_event_detector_handler(
		key_press_event_detector_st *const detector,
		key_press_handler_ft *handler, void *arg) __gcc_nonnull_args(1);

/*
 * przetwarza zarejestrowane zadania
 * musi być wywoływana w pętli głównej programu
 *
 * @detector	wskaźnik do detektora
 */
void key_press_event_detector_dispatch(
		key_press_event_detector_st *const detector) __gcc_nonnull;

/*
 * uaktualnia stan detektora
 * funkcja powinna być wywoływana cyklicznie, okres pomiędzy wywołaniami
 * tej funkcji definiuje jednostkę czasu
 *
 * @detector		wskaźnik do detektora
 * @key_is_pressed	sprzętowy stan przycisku
 */
void key_press_event_detector_on_tick_time(
		key_press_event_detector_st *const detector,
		bool key_is_pressed) __gcc_nonnull;

