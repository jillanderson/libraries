#pragma once

/** \file **********************************************************************
 *
 * wybrane atrybuty kompilatora gcc
 *
 ******************************************************************************/

/*
 * atrybuty_instrukcji
 */
#define __gcc_fallthrough	__attribute__((__fallthrough__))

/*
 * atrybuty_funkcji
 */
#define __gcc_alias(symbol)	__attribute__((__alias__(#symbol)))
#define __gcc_static_inline	static inline __attribute__((__always_inline__))
#define __gcc_always_inline	inline __attribute__((__always_inline__))
#define __gcc_gnu_inline	__attribute__((__gnu_inline__))
#define __gcc_noinline		__attribute__((__noinline__))
#define __gcc_cold		__attribute__((__cold__))
#define __gcc_hot		__attribute__((__hot__))
#define __gcc_const_attr	__attribute__((__const__))
#define __gcc_pure		__attribute__((__pure__))
#define __gcc_flatten		__attribute__((__flatten__))
#define __gcc_nonnull		__attribute__((__nonnull__))
#define __gcc_nonnull_args(...)	__attribute__((__nonnull__(__VA_ARGS__)))
#define __gcc_returns_nonnull	__attribute__((__returns_nonnull__))
#define __gcc_noreturn		__attribute__((__noreturn__))
#define __gcc_weak		__attribute__((__weak__))
#define __gcc_must_check	__attribute__((__warn_unused_result__))

/*
 * The constructor attribute causes the function to be called automatically
 * before execution enters main ().
 * Similarly, the destructor attribute causes the function to be called
 * automatically after main () has completed or exit () has been called.
 * Functions with these attributes are useful for initializing data that
 * will be used implicitly during the execution of the program.
 */
#define __gcc_constructor	__attribute__((__constructor__))
#define __gcc_destructor	__attribute__((__destructor__))

/*
 * atrybuty_funkcji_avr
 */
#define __gcc_naked		__attribute__((__naked__))
#define __gcc_os_main		__attribute__((__OS_main__))
#define __gcc_os_task		__attribute__((__OS_task__))

/*
 * atrybuty_wspólne_funkcji_i_zmiennych
 */
#define __gcc_always_unused	__attribute__((__unused__))
#define __gcc_maybe_unused	__attribute__((__unused__))
#define __gcc_used		__attribute__((__used__))

/*
 * atrybuty_zmiennych
 */
#define __gcc_packed		__attribute__((__packed__))
#define __gcc_io(addr)		__attribute__ ((io (addr)))

