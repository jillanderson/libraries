#pragma once

/** \file **********************************************************************
 *
 * nieblokująca obsługa sprzętowego interfejsu TWI w trybie MT/MR
 *
 ******************************************************************************/

#include <common/gcc_attributes.h>
#include <stddef.h>
#include <stdint.h>

enum {
	TWI_SPEED_100KHZ = 100,
	TWI_SPEED_400KHZ = 400
};

typedef enum {
	TWI_BUS_INITIALIZED,
	TWI_BUS_SUCCESS,
	TWI_BUS_BUSY,
	TWI_BUS_ERROR,
	TWI_BUS_SLAVE_ERROR
} twi_bus_status_et;

typedef void twi_handler_ft(uint8_t slave_addr, uint8_t *data, size_t size);

/*
 * inicjalizacja magistrali twi
 *
 * @speed_kHz	szybkość magistrali w kHz
 */
void twi_init(uint16_t speed_kHz);

/*
 * sprawdza status ostatniej transmisji
 *
 * @ret		status transmisji
 */
twi_bus_status_et twi_check_status(void);

/*
 * zapisuje dane w urządzeniu slave
 * funkcja kończy działanie niezwłocznie po rozpoczęciu transmisji
 *
 * @slave_addr	adres urządzenia
 * @word_addr	adres pierwszego zapisywanego rejestru
 * @data	dane do zapisania
 * @size	ilość danych do zapisania
 * @ret		status transmisji
 */
twi_bus_status_et twi_write_data(uint8_t slave_addr, uint8_t word_addr,
                uint8_t *data, size_t size, twi_handler_ft write_handler) __gcc_nonnull_args(3);

/*
 * odczytuje dane z urządzeni slave
 * funkcja kończy działanie niezwłocznie po rozpoczęciu transmisji
 *
 * @slave_addr	adres urządzenia
 * @word_addr	adres pierwszego odczytywanego rejestru
 * @data	bufor na odczytane dane
 * @size	ilość danych do odczytania
 * @ret		status transmisji
 */
twi_bus_status_et twi_read_data(uint8_t slave_addr, uint8_t word_addr,
                uint8_t *data, size_t size, twi_handler_ft read_handler) __gcc_nonnull_args(3);

