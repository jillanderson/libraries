#pragma once

/** \file **********************************************************************
 *
 * nieblokująca obsługa pamięci EEPROM
 *
 ******************************************************************************/

#include <common/gcc_attributes.h>
#include <stddef.h>

typedef enum {
	EEMEM_OK,
	EEMEM_BUSY,
	EEMEM_OVF
} eemem_result_et;

typedef void eemem_handler_ft(void *arg);

/*
 * funkcja nieblokująca, odczytuje blok n bajtów z adresu src pamięci EEPROM
 * i zapisuje do pamięci SRAM od adresu dst
 *
 * @src		adres początku bloku EEPROM
 * @dst		adres początku bloku w SRAM
 * @size	ilość bajtów do odczytania
 * @handler	funkcja zwrotna wywoływana po zakończeniu odczytu danych
 * @arg		wskaźnik do argumentu funkcji zwrotnej
 * @ret		EEPROM_OK, odczyt zakończony sukcesem, zakończenie odczytu jest
 * 		komunikowane przez funkcję zwrotną
 */
eemem_result_et eemem_read(void *restrict dst, void const *restrict src,
                size_t size, eemem_handler_ft *handler, void *arg) __gcc_nonnull_args(1,2);

/*
 * funkcja nieblokująca, zapisuje blok n bajtów z pamięci SRAM od adresu src
 * i zapisuje od adresu dst w pamięci EEPROM
 *
 * @src		wskaźnik początku bloku danych do zapisania
 * @dst		wskaźnik początku bloku w EEPROM
 * @size	ilość bajtów do zapisania
 * @handler	funkcja zwrotna wywoływana po zakończeniu zapisu danych
 * @arg		wskaźnik do argumentu funkcji zwrotnej
 * @ret		EEPROM_OK, zapis zakończony sukcesem, zakończenie zapisu jest
 * 		komunikowane przez funkcję zwrotną
 */
eemem_result_et eemem_write(void const *restrict src, void *restrict dst,
                size_t size, eemem_handler_ft *handler, void *arg) __gcc_nonnull_args(1,2);

