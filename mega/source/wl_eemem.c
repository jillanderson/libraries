#include <avr/io.h>
#include <common/crc8.h>
#include <common/gcc_attributes.h>
#include <mega/eemem.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <string.h>
#include "../wl_eemem.h"

static struct {
	uint8_t sentinel;
	uint8_t crc8;
	struct {
		void *address;
		uint8_t size;
	} data;

	eemem_handler_ft *write_complete_handler;
	void *arg;
} wl = { 0 };

static uint8_t const SENTINEL_MASK = (1U << (sizeof(wl.sentinel) * 8U - 1U));
/* umożliwia przekazanie adresu o wartości 0 bez rozwinięcia do NULL */
static uint8_t const *ADDR_0 = 0U;

static uintptr_t find_read_address(void);

void wl_init(void *data, uint8_t size)
{
	uintptr_t read_address = find_read_address();
	eemem_read(&(wl.sentinel), (uintptr_t*) read_address,
	                sizeof(wl.sentinel), NULL, NULL);
	(wl.data).address = data;
	(wl.data).size = size;
}

void wl_read_data(void)
{
	/* sentinel + crc8 + data */
	size_t const ENTRY_SIZE = (sizeof(wl.sentinel) + sizeof(wl.crc8)
	                + (wl.data).size);
	uint8_t wl_buffer[ENTRY_SIZE];
	uintptr_t read_address = find_read_address();
	eemem_read(wl_buffer, (uintptr_t*) read_address, ENTRY_SIZE, NULL,
	                NULL);
	wl.crc8 = wl_buffer[1U];
	memcpy((wl.data).address, &wl_buffer[2U], (wl.data).size);
}

__gcc_pure bool wl_read_data_is_valid(void)
{
	return (crc8_is_match((wl.data).address, (wl.data).size, wl.crc8));
}

void wl_write_data(void)
{

	/* sentinel + crc8 + data */
	size_t const ENTRY_SIZE = (sizeof(wl.sentinel) + sizeof(wl.crc8)
	                + (wl.data).size);
	uint8_t wl_buffer[ENTRY_SIZE];
	uintptr_t write_address = (find_read_address() + ENTRY_SIZE);

	if ((E2END + 1U) < (write_address + ENTRY_SIZE)) {
		write_address = (uintptr_t) ADDR_0;
		wl.sentinel ^= SENTINEL_MASK;
	}

	wl_buffer[0U] = wl.sentinel;
	wl_buffer[1U] = crc8_generate((wl.data).address, (wl.data).size);

	memcpy(&wl_buffer[2U], (wl.data).address, (wl.data).size);
	eemem_write(wl_buffer, (uintptr_t*) write_address, ENTRY_SIZE,
	                wl.write_complete_handler, wl.arg);
}

void wl_write_data_complete_handler(eemem_handler_ft handler, void *arg)
{
	wl.write_complete_handler = handler;
	wl.arg = arg;
}

/* po resecie pamięci EEPROM wszystkie komórki mają wartości 0xFFU */
uintptr_t find_read_address(void)
{
	/* sentinel + crc8 + data */
	size_t const ENTRY_SIZE = (sizeof(wl.sentinel) + sizeof(wl.crc8)
	                + (wl.data).size);
	uint16_t const ENTRY_QUANTITY = ((E2END + 1U) / ENTRY_SIZE);
	uint8_t sentinel_tmp;

	eemem_read(&sentinel_tmp, ADDR_0, sizeof(sentinel_tmp), NULL, NULL);

	uint8_t sentinel_first = sentinel_tmp & SENTINEL_MASK;

	/* wyszukiwanie binarne */
	uintptr_t left = (uintptr_t) ADDR_0;
	uintptr_t right = (uintptr_t) ENTRY_QUANTITY;

	while ((left + 1U) != right) {
		uintptr_t mid = left + (right - left) / 2U;

		eemem_read(&sentinel_tmp, (uintptr_t*) (mid * ENTRY_SIZE),
		                sizeof(sentinel_tmp), NULL, NULL);

		(sentinel_first != (sentinel_tmp & SENTINEL_MASK)) ?
		                (right = mid) : (left = mid);
	}

	return (left * ENTRY_SIZE);
}

