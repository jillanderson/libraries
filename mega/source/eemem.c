#include <avr/interrupt.h>
#include <avr/io.h>
#include <common/gcc_attributes.h>
#include <mega/eemem.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

static struct {
	eemem_handler_ft *w_handler;
	void *arg;
	volatile size_t size;
	volatile uint8_t const *src_p;
	volatile uint8_t *dst_p;
} ee_transfer = { 0 };

/*
 * sprzętowy odczyt wartości komórki EEPROM
 * nie sprawdza gotowości pamięci
 *
 * @addr	adress komórki EEPROM
 */
__gcc_static_inline uint8_t ee_read_byte(uintptr_t addr);

/*
 * sprzętowy zapis podanej wartości do komórki EEPROM
 * nie sprawdza gotowości pamięci
 * włącza przerwanie EE_RDY_vect
 *
 * @addr	adress komórki EEPROM
 * @data	wartość do zapisania
 */
__gcc_static_inline void ee_write_byte(uintptr_t addr, uint8_t data);

/*
 * włącza przerwanie EE_RDY_vect
 */
__gcc_static_inline void ee_isr_enable(void);

/*
 * wyłącza przerwanie EE_RDY_vect
 */
__gcc_static_inline void ee_isr_disable(void);

/*
 * testuje czy pamięć jest zajęta
 */
__gcc_static_inline bool ee_is_busy(void);

eemem_result_et eemem_read(void *restrict dst, void const *restrict src,
                size_t size, eemem_handler_ft *r_handler, void *arg)
{
	eemem_result_et result = EEMEM_OK;

	if ((E2END + 1U) < ((uintptr_t) src + size)) {
		result = EEMEM_OVF;
	} else if (true == ee_is_busy()) {
		result = EEMEM_BUSY;
	} else {
		uint8_t *dst_p = dst;
		uint8_t const *src_p = src;

		while (0U < size--) {
			*(dst_p++) = ee_read_byte((uintptr_t) (src_p++));
		}

		if (NULL != r_handler) {
			r_handler(arg);
		}
	}

	return (result);
}

eemem_result_et eemem_write(void const *restrict src, void *restrict dst,
                size_t size, eemem_handler_ft *w_handler, void *arg)
{
	eemem_result_et result = EEMEM_OK;

	if ((E2END + 1U) <= ((uintptr_t) dst + size)) {
		result = EEMEM_OVF;
	} else if (true == ee_is_busy()) {
		result = EEMEM_BUSY;
	} else {
		ee_transfer.w_handler = w_handler;
		ee_transfer.arg = arg;
		ee_transfer.size = size;
		ee_transfer.dst_p = dst;
		ee_transfer.src_p = src;

		ee_isr_enable();
	}

	return (result);
}

ISR(EE_RDY_vect)
{
	if (0U < ee_transfer.size--) {
		uint8_t src_data = *(ee_transfer.src_p++);
		uint8_t ee_data = ee_read_byte((uintptr_t) (ee_transfer.dst_p));

		if (ee_data != src_data) {
			ee_write_byte((uintptr_t) (ee_transfer.dst_p),
			                src_data);
		}

		ee_transfer.dst_p++;
	} else {
		ee_isr_disable();

		if (NULL != ee_transfer.w_handler) {
			(ee_transfer.w_handler)(ee_transfer.arg);
		}
	}
}

uint8_t ee_read_byte(uintptr_t addr)
{
	EEAR = addr;
	EECR |= (1U << EERE);

	return (EEDR);
}

void ee_write_byte(uintptr_t addr, uint8_t data)
{
	EEAR = addr;
	EEDR = data;
	EECR |= (1U << EEMWE);
	EECR |= (1U << EEWE);
}

void ee_isr_enable(void)
{
	EECR |= (1U << EERIE);
}

void ee_isr_disable(void)
{
	EECR &= (uint8_t) (~(1U << EERIE));
}

bool ee_is_busy(void)
{
	return ((0U == (EECR & (1 << EEWE))) ? false : true);
}

