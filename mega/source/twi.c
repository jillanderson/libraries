#include <avr/interrupt.h>
#include <avr/io.h>
#include <common/gcc_attributes.h>
#include <mega/twi.h>
#include <stddef.h>
#include <stdint.h>
#include <util/twi.h>

static struct {
	twi_handler_ft *callback;

	volatile enum {
		TWI_MODE_IDLE,
		TWI_MODE_WRITE,
		TWI_MODE_READ
	} mode;

	uint8_t slave_addr;
	uint8_t word_addr;
	uint8_t *data;
	size_t size;
	volatile size_t index;
	volatile twi_bus_status_et status;
} twi = { 0 };

__gcc_static_inline void bus_enable(void);
__gcc_static_inline void bus_start(void);
__gcc_static_inline void bus_repeat_start(void);
__gcc_static_inline void bus_stop(void);
__gcc_static_inline void bus_read_ack(void);
__gcc_static_inline void bus_read_nack(void);
__gcc_static_inline void bus_transmit(void);

__gcc_static_inline void data_send(uint8_t data);
__gcc_static_inline uint8_t data_receive(void);
__gcc_static_inline void data_read_reply(void);

__gcc_static_inline uint8_t address_sla_w(uint8_t slave_addr);
__gcc_static_inline uint8_t address_sla_r(uint8_t slave_addr);

__gcc_static_inline void success_handle(void);

/*
 * TWSR - TWI Status Register
 *
 * TWPS1 TWPS0
 * 0 	0 	clk T2S /1 (No prescaling)
 * 0 	1 	clk T2S /4 (From prescaler)
 * 1 	0 	clk T2S /16 (From prescaler)
 * 1 	1 	clk T2S /64 (From prescaler)
 */
void twi_init(uint16_t speed_kHz)
{
	static uint8_t const divisor = 4U;

	/* speed_kHz = TWBR * 4^TWPS */
	uint16_t bit_rate_division = (uint8_t) (((F_CPU / 1000U) / speed_kHz)
	                - 16U) / 2U;

	uint16_t prescaler = 0U;

	while (UINT8_MAX < bit_rate_division) {
		prescaler++;
		bit_rate_division = (bit_rate_division / divisor);
	};

	TWSR =
	                (uint8_t) ((TWSR & ~((1U << TWPS1) | (1U << TWPS0)))
	                                | prescaler);
	TWBR = (uint8_t) bit_rate_division;

	bus_enable();

	twi.mode = TWI_MODE_IDLE;
	twi.status = TWI_BUS_INITIALIZED;
}

twi_bus_status_et twi_check_status(void)
{
	twi_bus_status_et result = TWI_BUS_BUSY;

	if (TWI_MODE_IDLE == twi.mode) {
		result = twi.status;
		twi.status = TWI_BUS_INITIALIZED;
	}

	return (result);
}

twi_bus_status_et twi_write_data(uint8_t slave_addr, uint8_t word_addr,
                uint8_t *data, size_t size, twi_handler_ft write_handler)
{
	twi_bus_status_et result = TWI_BUS_BUSY;

	if (TWI_MODE_IDLE == twi.mode) {
		twi.callback = write_handler;
		twi.slave_addr = slave_addr;
		twi.word_addr = word_addr;
		twi.data = data;
		twi.size = size;
		twi.index = 0U;
		twi.mode = TWI_MODE_WRITE;
		twi.status = TWI_BUS_BUSY;

		result = TWI_BUS_SUCCESS;
		bus_start();
	}

	return (result);
}

twi_bus_status_et twi_read_data(uint8_t slave_addr, uint8_t word_addr,
                uint8_t *data, size_t size, twi_handler_ft read_handler)
{
	twi_bus_status_et result = TWI_BUS_BUSY;

	if (TWI_MODE_IDLE == twi.mode) {
		twi.callback = read_handler;
		twi.slave_addr = slave_addr;
		twi.word_addr = word_addr;
		twi.data = data;
		twi.size = size;
		twi.index = 0U;
		twi.mode = TWI_MODE_READ;
		twi.status = TWI_BUS_BUSY;

		result = TWI_BUS_SUCCESS;
		bus_start();
	}

	return (result);
}

ISR(TWI_vect)
{
	switch (TW_STATUS) {
	case TW_START:
		data_send(address_sla_w(twi.slave_addr));
		bus_transmit();
		break;

	case TW_MT_SLA_ACK:
		data_send(twi.word_addr);
		bus_transmit();
		break;

	case TW_MT_DATA_ACK:
		switch (twi.mode) {
		case TWI_MODE_WRITE:
			if (twi.index < twi.size) {
				data_send((twi.data[twi.index]));
				twi.index++;
				bus_transmit();
			} else {
				twi.mode = TWI_MODE_IDLE;
				twi.status = TWI_BUS_SUCCESS;
				bus_stop();
				success_handle();
			}
			break;

		case TWI_MODE_READ:
			bus_repeat_start();
			break;

		default:
			;
			break;
		}
		break;

	case TW_MT_SLA_NACK:
	case TW_MT_DATA_NACK:
	case TW_MR_SLA_NACK:
		twi.mode = TWI_MODE_IDLE;
		twi.status = TWI_BUS_SLAVE_ERROR;
		bus_stop();
		break;

	case TW_REP_START:
		data_send(address_sla_r(twi.slave_addr));
		bus_transmit();
		break;

	case TW_MR_SLA_ACK:
		data_read_reply();
		break;

	case TW_MR_DATA_ACK:
		twi.data[twi.index] = data_receive();
		twi.index++;
		data_read_reply();
		break;

	case TW_MR_DATA_NACK:
		twi.data[twi.index] = data_receive();
		twi.mode = TWI_MODE_IDLE;
		twi.status = TWI_BUS_SUCCESS;
		bus_stop();
		success_handle();
		break;

	default:
		twi.mode = TWI_MODE_IDLE;
		twi.status = TWI_BUS_ERROR;
		bus_stop();
		break;
	}
}

void bus_enable(void)
{
	TWCR = (1 << TWEN);
}

void bus_start(void)
{
	TWCR = ((1 << TWINT) | (1 << TWSTA) | (1 << TWEN) | (1 << TWIE));
}

void bus_repeat_start(void)
{
	bus_start();
}

void bus_stop(void)
{
	TWCR = ((1 << TWINT) | (1 << TWSTO) | (1 << TWEN) | (1 << TWIE));
}

void bus_read_ack(void)
{
	TWCR = ((1 << TWINT) | (1 << TWEA) | (1 << TWEN) | (1 << TWIE));
}

void bus_read_nack(void)
{
	TWCR = ((1 << TWINT) | (1 << TWEN) | (1 << TWIE));
}

void bus_transmit(void)
{
	TWCR = ((1 << TWINT) | (1 << TWEN) | (1 << TWIE));
}

void data_send(uint8_t data)
{
	TWDR = data;
}

uint8_t data_receive(void)
{
	return (TWDR);
}

void data_read_reply(void)
{
	if (twi.index < (twi.size - 1U)) {
		bus_read_ack();
	} else {
		bus_read_nack();
	}
}

uint8_t address_sla_w(uint8_t slave_addr)
{
	return ((uint8_t) (slave_addr << 1U) + TW_WRITE);
}

uint8_t address_sla_r(uint8_t slave_addr)
{
	return ((uint8_t) ((slave_addr << 1U) + TW_READ));
}

void success_handle(void)
{
	if (NULL != twi.callback) {
		twi.status = TWI_BUS_INITIALIZED;
		(twi.callback)(twi.slave_addr, twi.data, twi.size);
	}
}

