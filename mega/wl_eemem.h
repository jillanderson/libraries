#pragma once

/** \file **********************************************************************
 *
 * nieblokujący zapis i odczyt danych w pamięci EEPROM, implementuje technikę
 * "wear leveling EEPROM", wykorzystuje całą dostępną pamięć - adresy 0-E2END
 * testuje integralność danych sumą kontrolną crc8
 *
 ******************************************************************************/

#include <common/gcc_attributes.h>
#include <mega/eemem.h>
#include <stdbool.h>
#include <stdint.h>

/*
 * inicjuje zapis ustawień w pamięci EEPROM
 *
 * @data	bufor danych zapisu - odczytu
 * @size	rozmiar bloku danych
 */
void wl_init(void *data, uint8_t size) __gcc_nonnull;

/*
 * odczytuje dane z pamięci eeprom do zdefiniowanego bufora
 *
 * @ret		WL_OK, odczyt zakończony sukcesem
 */
void wl_read_data(void);

/*
 * sprawdza czy odczytane dane są poprawne
 *
 * @ret		true - dane poprawne, kod crc8 zgodny
 */
bool wl_read_data_is_valid(void);

/*
 * zapisuje dane ze zdefiniowanego bufora do pamięci eeprom
 *
 * @ret		WL_OK, zapis zakończony sukcesem
 */
void wl_write_data(void);

/*
 * rejestruje funkcję wywoływaną po zakończeniu zapisu danych pamięci EEPROM
 *
 * @handler	funkcja zwrotna wywoływana po zakończeniu zapisu danych
 * @arg		wskaźnik do argumentu funkcji zwrotnej
 */
void wl_write_data_complete_handler(eemem_handler_ft handler, void *arg);

