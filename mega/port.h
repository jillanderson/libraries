#pragma once

/** \file **********************************************************************
 *
 * konfiguracja i obsługa portów gpio
 *
 ******************************************************************************/

#include <stdint.h>

#include "../common/gcc_attributes.h"

__gcc_static_inline __gcc_nonnull void port_set_value(
                volatile uint8_t *const port, uint8_t value)
{
	*port = (value);
}

__gcc_static_inline __gcc_nonnull void port_set_mask(
                volatile uint8_t *const port, uint8_t mask)
{
	*port |= (mask);
}

__gcc_static_inline __gcc_nonnull void port_clear_mask(
                volatile uint8_t *const port, uint8_t mask)
{
	*port &= (uint8_t) (~(mask));
}

__gcc_static_inline __gcc_nonnull void port_set_mask_opposite(
                volatile uint8_t *const port, uint8_t mask)
{
	*port ^= (mask);
}

__gcc_static_inline __gcc_nonnull uint8_t port_read_value(
                volatile uint8_t *const port)
{
	return (*port);
}

