#pragma once

/** \file **********************************************************************
 *
 * konfiguracja i obsługa pinów gpio
 *
 ******************************************************************************/

#include <stdint.h>

#include "../common/gcc_attributes.h"

typedef struct {
	volatile uint8_t *port;
	uint8_t pin;
} io_pin_st;

__gcc_static_inline __gcc_nonnull volatile uint8_t* port_ddrx_from_portx(
                volatile uint8_t *const px)
{
	return (px - 1U);
}

__gcc_static_inline __gcc_nonnull volatile uint8_t* port_pinx_from_portx(
                volatile uint8_t *const px)
{
	return (px - 2U);
}

__gcc_static_inline __gcc_nonnull void io_configure_as_output(
                io_pin_st const *const io)
{
	*(port_ddrx_from_portx(io->port)) |= (uint8_t) (1U << (io->pin));
}

__gcc_static_inline __gcc_nonnull void io_configure_as_input(
                io_pin_st const *const io)
{
	*(port_ddrx_from_portx(io->port)) &= (uint8_t) (~(1U << (io->pin)));
}

__gcc_static_inline __gcc_nonnull void io_set_high(io_pin_st const *const io)
{
	*(io->port) |= (uint8_t) (1U << (io->pin));
}

__gcc_static_inline __gcc_nonnull void io_set_low(io_pin_st const *const io)
{
	*(io->port) &= (uint8_t) (~(1U << (io->pin)));
}

__gcc_static_inline __gcc_nonnull void io_set_opposite(
                io_pin_st const *const io)
{
	*(io->port) ^= (uint8_t) (1U << (io->pin));
}

__gcc_static_inline __gcc_nonnull uint8_t io_read_pin(io_pin_st const *const io)
{
	return ((uint8_t) ((*(port_pinx_from_portx(io->port))) >> (io->pin))
	                & 1U);
}

